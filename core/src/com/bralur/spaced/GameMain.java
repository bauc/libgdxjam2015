package com.bralur.spaced;

import com.badlogic.gdx.Game;

public class GameMain extends Game
    {

        TestScreen testScreen;

        @Override
        public void create()
            {

                testScreen = new TestScreen();

                this.setScreen(testScreen);
            }

    }
