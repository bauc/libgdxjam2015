package com.bralur.spaced;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;


public class Player extends Sprite
    {

        TextureRegion[] upFrames;
        TextureRegion currentFrame;
        float animSpeed = 1 / 5f;
        Animation up;

        Texture tex;

        Texture spot;

        OrthographicCamera cam;

        float moveX = 0;
        float moveY = 0;
        float speed = 0;

        int newScreenX = 0;
        int newScreenY = 0;

        Vector3 mouse = new Vector3();

        float rotation = 0;

        float dx;
        float dy;
        float vx;
        float vy;

        int targetX;
        int targetY;

        float veloX;
        float veloY;

        float markerX;
        float markerY;

        float stateTime = 0;

        public void setTargetX(int x, int y)
            {
                targetX = x;
                targetY = y;
            }

        public int getTargetX()
            {
                return targetX;
            }

        public int getTargetY()
            {
                return targetY;
            }

        public Player(Texture t, OrthographicCamera cam)
            {

                Pixmap plop = new Pixmap(10, 10, Pixmap.Format.RGBA8888);
                plop.setColor(Color.WHITE);
                plop.fillCircle(5, 5, 4);

                spot = new Texture(plop);

                tex = t;

                //TextureRegion[][] tmp = TextureRegion.split(tex, tex.getWidth(), tex.getHeight());

                //upFrames = new TextureRegion[tmp[0].length];

                int index = 0;

                //System.out.println("Length of tmp: " + String.valueOf(tmp[0].length));

                /*for (int i = 0; i < tmp[0].length; i++)
                    {
                        upFrames[i] = tmp[0][i];
                        //index++;

                    }

                up = new Animation(animSpeed, upFrames);
                up.setPlayMode(Animation.PlayMode.LOOP);

                currentFrame = up.getKeyFrame(stateTime, true);
                */
                this.set(new Sprite(t));
                this.setOrigin(t.getWidth() / 2, t.getHeight() / 2);

                this.setScale(0.2f);

                this.cam = cam;
            }

        public void update(float dt)
            {
                stateTime += dt;

                //currentFrame = up.getKeyFrame(stateTime, true);

                if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT))
                    {
                        speed = 20;
                    }
                else
                    {
                        speed = 10;
                    }

                moveX = 0;
                moveY = 0;

                newScreenX = Gdx.input.getX();
                newScreenY = Gdx.input.getY();

                mouse.set(newScreenX, newScreenY, 0);
                cam.unproject(mouse);

                int deltaX = (int) (mouse.x - getX());
                int deltaY = (int) (mouse.y - getY());


                //rotation = (MathUtils.atan2(deltaY, deltaX) * 180 / MathUtils.PI)-90;//

                if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
                    {
                        rotation += 2;
                    }

                if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
                    {
                        rotation -= 2;
                    }

                if (rotation < 0)
                    {
                        rotation = 360 + rotation;
                        System.out.println("Resetting rotation");
                    }
                if (rotation > 360)
                    {
                        rotation = rotation - 360;
                        System.out.println("Resetting rotation");
                    }

                if (Gdx.input.isKeyPressed(Input.Keys.W))
                    {
                        dx = MathUtils.cosDeg(rotation - 90);
                        dy = MathUtils.sinDeg(rotation - 90);

                        vx = dx * speed;
                        vy = dy * speed;

                        veloX = veloX - (vx * dt);
                        veloY = veloY - (vy * dt);

                    }
                if (Gdx.input.isKeyPressed(Input.Keys.S))
                    {
                        dx = MathUtils.cosDeg(rotation - 270);
                        dy = MathUtils.sinDeg(rotation - 270);

                        vx = dx * speed;
                        vy = dy * speed;

                        veloX = veloX - (vx * dt);
                        veloY = veloY - (vy * dt);

                    }

                if (Gdx.input.isKeyPressed(Input.Keys.SPACE))
                    {
                        veloX -= (veloX / 10);
                        veloY -= (veloY / 10);
                    }

                this.setRotation(rotation);

                this.moveX += veloX;
                this.moveY += veloY;

                this.translate(moveX, moveY);


                if (targetOnScreen())
                    {
                        //markerX = 0;
                        //markerY = 0;
                    }
                else
                    {

                        //rotation = (MathUtils.atan2(deltaY, deltaX) * 180 / MathUtils.PI)-90;//

                        //float rota = (MathUtils.atan2(targetY , targetX ) * 180 / MathUtils.PI);
                        //float rota = (MathUtils.atan2(getY()- targetY, getX()- targetX ) * 180 / MathUtils.PI)-90;

                        float rota = (MathUtils.atan2(getY() - targetY, getX() - targetX) * 180 / MathUtils.PI) - 180;

                        markerX = this.getWidth() / 2 + getX() + MathUtils.cosDeg(rota) * 250;
                        markerY = this.getHeight() / 2 + getY() + MathUtils.sinDeg(rota) * 250;

                        /*if (targetY == 0)
                            {
                                m = 0;
                            }
                        else
                            {
                                m = targetX / targetY;
                            }
                        markerX = m * getX();
                        markerY = m * getY();

                        Intersector t;

                        t.int*/
                    }


            }

        float m;

        public boolean targetOnScreen()
            {


                return (

                        (targetX > getX() - 320) &&
                                (targetX < getX() + 320) &&
                                (targetY > getY() - 240) &&
                                (targetY < getY() + 240));

            }

        public void render(SpriteBatch batch)
            {
                //batch.draw(currentFrame, getX(), getY(), tex.getWidth() / 2, tex.getHeight() / 2, tex.getWidth(), tex.getHeight(), this.getScaleX(), this.getScaleY(), rotation);

                draw(batch);

                if (!targetOnScreen())
                    {
                        batch.draw(spot, markerX, markerY);
                    }

            }

    }
