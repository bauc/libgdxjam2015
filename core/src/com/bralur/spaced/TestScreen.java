package com.bralur.spaced;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;

public class TestScreen implements Screen
    {

        BitmapFont font;

        Planet[] planets;
        Asteroid[] asteroids;

        Player playerShip;

        Texture starS;
        TextureRegion[] theStars;
        int[][] bunchOfStars;

        SpriteBatch batch;
        OrthographicCamera cam;

        OrthographicCamera hudCam;

        private int numStars = 6500;
        private int numPlanets = 5;
        private int numAsteroids = 50;

        public void create()
            {
                batch = new SpriteBatch();
                cam = new OrthographicCamera(640, 480);
                cam.translate(cam.viewportWidth / 2, cam.viewportHeight / 2);
                cam.zoom++;
                cam.update();

                hudCam = new OrthographicCamera(640, 480);
                hudCam.translate(hudCam.viewportWidth / 2, hudCam.viewportHeight / 2);
                hudCam.update();

                font = new BitmapFont();

                planets = new Planet[numPlanets];

                for (int i = 0; i < numPlanets; i++)
                    {
                        planets[i] = new Planet();
                    }

                asteroids = new Asteroid[numAsteroids];

                for (int i = 0; i < numAsteroids; i++)
                    {
                        asteroids[i] = new Asteroid();
                    }

                Pixmap a = new Pixmap(5, 3, Pixmap.Format.RGBA8888);

                a.setColor(Color.WHITE);
                a.drawPixel(1, 0);
                a.drawPixel(1, 1);
                a.drawPixel(1, 2);
                a.drawPixel(1, 1);
                a.drawPixel(2, 1);

                a.setColor(Color.LIGHT_GRAY);
                a.drawPixel(1, 0);

                a.setColor(Color.DARK_GRAY);
                a.drawPixel(2, 0);

                starS = new Texture(a);

                a.dispose();

                theStars = new TextureRegion[3];
                theStars[0] = new TextureRegion(starS, 0, 0, 2, 2);
                theStars[1] = new TextureRegion(starS, 3, 0, 1, 1);
                theStars[2] = new TextureRegion(starS, 4, 0, 1, 1);

                bunchOfStars = new int[numStars][3];

                for (int i = 0; i < numStars; i++)
                    {
                        bunchOfStars[i][0] = MathUtils.random(-3400, 7000);
                        bunchOfStars[i][1] = MathUtils.random(-3800, 7000);
                        bunchOfStars[i][2] = MathUtils.random(0, 2);
                    }

                playerShip = new Player(new Texture(Gdx.files.internal("alienship_new.png")), cam);
                //playerShip.setScale(0.2f);

            }

        @Override
        public void show()
            {

                create();

            }

        public void update(float dt)
            {

                playerShip.update(dt);


                if (Gdx.input.isKeyPressed(Input.Keys.T))
                    {
                        int targetAsteroid = MathUtils.random(numAsteroids-1);

                        playerShip.setTargetX((int)asteroids[targetAsteroid].getX(), (int)asteroids[targetAsteroid].getY());

                    }


            }

        public void camFollowPlayer(float dt)
            {

                float camX = playerShip.getX() - cam.position.x + 320;
                float camY = playerShip.getY() - cam.position.y + 240;

                cam.translate(camX, camY);

                cam.update();

            }

        @Override
        public void render(float delta)
            {

                update(delta);

                camFollowPlayer(delta);

                Gdx.gl.glClearColor(0, 0, 0, 1);
                Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
                batch.setProjectionMatrix(cam.combined);
                batch.begin();

                for (int i = 0; i < numStars; i++)
                    {
                        batch.draw(theStars[bunchOfStars[i][2]], bunchOfStars[i][0], bunchOfStars[i][1]);
                    }

                for (int i = 0; i < numPlanets; i++)
                    {
                        //planets[i].draw(batch);
                        planets[i].render(batch);
                    }

                for (int i = 0; i < numAsteroids; i++)
                    {
                        asteroids[i].render(batch);
                    }

                playerShip.render(batch);


                batch.end();

                batch.setProjectionMatrix(hudCam.combined);
                batch.begin();

                font.draw(batch, String.valueOf((int) playerShip.getX()) + ", " + String.valueOf((int) playerShip.getY()), 10, 15);
                font.draw(batch, "Target: Asteroid : " + String.valueOf(playerShip.getTargetX()) + ", " + String.valueOf(playerShip.getTargetY()), 250, 15);
                font.draw(batch, "Marker: x: " + String.valueOf(playerShip.markerX) + ", y: " + String.valueOf(playerShip.markerY + " target vis: " + String.valueOf(playerShip.targetOnScreen())), 10, 30);

                batch.end();

            }

        @Override
        public void resize(int width, int height)
            {

            }

        @Override
        public void pause()
            {

            }

        @Override
        public void resume()
            {

            }

        @Override
        public void hide()
            {

            }

        @Override
        public void dispose()
            {

            }
    }
