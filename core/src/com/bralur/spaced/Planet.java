package com.bralur.spaced;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

/**
 * Created by BRENDAN on 23/12/2015.
 */
public class Planet extends Sprite
    {

        Texture t;

        public Planet(String t)
            {
                super(new Texture(Gdx.files.internal("planets16/" + t)));
            }

        public Planet()
            {
                super();
                int thePlanet = MathUtils.random(18, 33);

                t = new Texture(Gdx.files.internal("planets16/planet_" + String.valueOf(thePlanet) + ".png"));

                this.set(new Sprite(t));


                int x = MathUtils.random(-5000, 5000);
                int y = MathUtils.random(-5000, 5000);

                setPosition(x, y);

            }

        public  void render(SpriteBatch batch)
            {
                //batch.draw(t, getX(), getY());
                draw(batch);
            }
    }
