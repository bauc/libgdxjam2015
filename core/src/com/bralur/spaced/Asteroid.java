package com.bralur.spaced;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

/**
 * Created by BRENDAN on 23/12/2015.
 */
public class Asteroid extends Sprite
    {

        Texture t;

        String[] possibleAsteroids = new String [] {"1346943991.png", "1346944555.png", "1346945276.png", "1346945753.png", "1346945972.png", "1346946235.png", "1346946509.png"};

        public Asteroid(String t)
            {
                super(new Texture(Gdx.files.internal("planets16/" + t)));
            }

        public Asteroid()
            {
                super();
                String theAsteroid = possibleAsteroids[MathUtils.random(possibleAsteroids.length-1)];

                t = new Texture(Gdx.files.internal("asteroid/" + theAsteroid));

                this.set(new Sprite(t));


                int x = MathUtils.random(-5000, 5000);
                int y = MathUtils.random(-5000, 5000);

                setPosition(x, y);
                setScale(MathUtils.random(0.0f, 0.7f));

            }

        public  void render(SpriteBatch batch)
            {
                //batch.draw(t, getX(), getY());
                draw(batch);
            }
    }
