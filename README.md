# README #

Entry for libGDX Jam 2015 (http://itch.io/jam/libgdxjam)

Import using gradle (based on standard libGDX set up)

Assets not included but can be downloaded from

* Planets: http://opengameart.org/content/16-planet-sprites
* Asteroids: http://opengameart.org/content/asteroid-generator-and-a-set-of-generated-asteroids
* Spaceship: http://opengameart.org/content/complete-spaceship-game-art-pack